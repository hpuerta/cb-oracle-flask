from crypt import methods
from multiprocessing import connection
from sqlite3 import Cursor
from flask import Flask,jsonify, redirect,render_template,request
import cx_Oracle


def create_app():
    app = Flask(__name__)

    def connect():
        return cx_Oracle.connect(user='hpuerta',password='hpuerta',dsn="oracle:1521/xe")

    @app.route("/hola/")
    def hola():
        return "Hola"

    @app.route("/todos/")
    def index():
        connection = connect()
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM todo")
        ######
        cursor.rowfactory = lambda *args: dict(zip([d[0] for d in cursor.description],args))
        ######
        rows = cursor.fetchall()
        connection.close()
        #return jsonify(rows), 200
        return render_template('todos/index.html',todos=rows)

    @app.route("/todos/create/")
    def create():
        return render_template('todos/create.html')

    @app.route("/todos/",methods=['POST'])
    def store():
        descripcion = request.form['descripcion']
        connection = connect()
        cursor = connection.cursor()
        connection.autocommit = True
        cursor.execute("INSERT INTO todo (descripcion) VALUES ('" + descripcion + "')")
        connection.close()
        return redirect('/todos')

    @app.route("/todos/<int:id>")
    def show(id):
        connection = connect()
        cursor = connection.cursor()
        cursor.execute( "SELECT * FROM todo WHERE id =" + str(id))
        row = cursor.fetchone()
        tarea = {
            'ID':row[0],
            'DESCRIPCION':row[1]
        }
        connection.close()
        return render_template('todos/show.html',todo=tarea)

    @app.route("/todos/<int:id>/edit/")
    def edit(id):
        connection = connect()
        cursor = connection.cursor()
        cursor.execute( "SELECT * FROM todo WHERE id =" + str(id))
        row = cursor.fetchone()
        tarea = {
            'ID':row[0],
            'DESCRIPCION':row[1]
        }
        connection.close()
        return render_template('todos/edit.html',todo=tarea)

    @app.route("/todos/<int:id>/update",methods=['POST'])
    def update(id):
        connection = connect()
        cursor = connection.cursor()
        connection.autocommit = True
        cursor.execute("UPDATE todo SET descripcion='" + request.form['descripcion'] +"' WHERE id = " + str(id))
        connection.close()
        return redirect("/todos/")

    @app.route("/todos/<int:id>/delete/",methods=['POST'])
    def delete(id):
        connection = connect()
        cursor = connection.cursor()
        connection.autocommit = True
        cursor.execute("DELETE FROM todo WHERE id = " + str(id))
        connection.close()
        #return "Borrado"
        return redirect("/todos")

    return app